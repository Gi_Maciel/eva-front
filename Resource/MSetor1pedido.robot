*** Settings ***
Library         SeleniumLibrary

*** Variables ***
${TELA_PCP}                          http://eva.noprod.ecom.carrefour/pcp
${CABEÇALHO}                         xpath=//*[text() = 'Controle PCP']
${FILTRO_PEDIDO}                     xpath=//div[@class="filter-group"]/div/div/input[contains(@id,"Pedido")]
${PEDIDO}                            2271995
${BOTAO_PESQUISAR}                   xpath=//button/div/div/span[text()="Pesquisar"]
${VALIDAR_PEDIDO}                    xpath=//tbody/tr/td[contains(text(),'227199511123')]
${CHECK}                             xpath=//tbody/tr/td/div/input[contains(@name,"0-cb")]
${BTN_POR_SETOR}                     xpath=//button[2][contains(@class,"eva-action-bar-submit primary btn-by-sector")]
${MENU_TAREFAS}                      xpath=//*[@class="side-menu-item"]/div/div/span[text()="Aberto" and not(contains(@title, 'Pedido')) ]
${PESQUISAR_FILTRO_DE_TAREFAS}       xpath=//div[@class="filter-group"]//div[7]/div/button
${CHECKBOX_TAREFAS}                  xpath=//input[contains(@name,"selectallcb")] 
${BTN_CRIAR}                         xpath=//button[2]/div/span[text()="CRIAR"]
${BTN_CLICAR_E_IMPRIMIR_LISTA}       xpath=//button[contains(@class,"eva-dialog-cancel-action ")]
${TAREFAS}                           xpath=//span[text()="Tarefas"]
${BTN_CONFIRMAR_MISSAO}              xpath=//button[contains(@class,"eva-action-bar-submit-warning primary")]
${ATRIBUIR_PICKER_GENERICO}          xpath=//label[contains(@class,"container-radio-button")]/i[contains(@class,"icon icon-input-radio-off")]

                          





*** Keywords ***
###CENARIO 1: Criar Tarefa por Setor com 1 pedido
Dado que esteja logado na tela de PCP
    Element Text Should Be       ${CABEÇALHO}   Controle PCP
  ###Page Should Contain      Controle PCP

E buscar por pedido no filtro de pedido
    Sleep       5s
    Input Text                  ${FILTRO_PEDIDO}   ${PEDIDO}
    Sleep       3s
    Click Element               ${BOTAO_PESQUISAR} 
    Log                         ${VALIDAR_PEDIDO}  

Quando selecionar pedido
    Select Checkbox             ${CHECK} 

E clicar em tarefa por Setor
    Click Button                ${BTN_POR_SETOR}  


###CENARIO 2: Criar Missao com 1 pedido
Dado que acesse menu de Tarefas
  Sleep       3s    
  Click Element                        ${MENU_TAREFAS}
   
E busque pelo pedido
  Sleep        2s
  Input Text                    ${FILTRO_PEDIDO}    ${PEDIDO}
  Click Element                 ${PESQUISAR_FILTRO_DE_TAREFAS}
  
E selecione o pedido
  Sleep        2s
  Select Checkbox                ${CHECKBOX_TAREFAS}
  Click Button                   ${BTN_CONFIRMAR_MISSAO}             

Quando clicar no botao Criar
  
  Click Element              ${BTN_CRIAR} 
   Sleep        5s

Então a Missao devera ser criada com sucesso

  Click Element             ${ATRIBUIR_PICKER_GENERICO}
  Sleep        3s
  Click Button             ${BTN_CLICAR_E_IMPRIMIR_LISTA}



