*** Settings ***
Library         SeleniumLibrary

*** Variables ***
${URL_INICIOEVA}        http://eva.noprod.ecom.carrefour/
${TELA_PCP}             http://eva.noprod.ecom.carrefour/pcp
${ICONE_PCP}            css=body div ul>li>a>i
${BOTAO_PCP}            css=body div ul>li>a 
${CABEÇALHO}            css=//div/div/div/div/div[3]/span[text()="Loja"]




*** Keywords ***
###CENARIO 1: Abrir a pagina PCP
Dado que esteja logado no Eva

    Title Should Be                   Carrefour
## Wait Until Element Is Visible   ${CABEÇALHO}
   
                
Quando clicar em PCP   
    Sleep                          5s
    #Element Should Be Focused      ${ICONE_PCP} 
    Click Element                   ${ICONE_PCP} 

Então deverá ser direcionado para a pagina de PCP 
    #Page Should Contain     //h1[text()="Controle PCP"]
     Page Should Contain      Controle PCP
     

     
