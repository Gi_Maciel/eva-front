*** Settings ***
Library         SeleniumLibrary
Resource        ../Resource/ResourceCommonLoginEva.robot
Resource        ../Resource/PCP.robot
Suite Setup      Open Browser To Login valid

*** Test Cases ***

CENARIO 1: Abrir a pagina PCP
    Dado que esteja logado no Eva
    Quando clicar em PCP
    Então deverá ser direcionado para a pagina de PCP
    E clicar em Tarefa por Pedido

