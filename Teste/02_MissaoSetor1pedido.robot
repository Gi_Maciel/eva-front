*** Settings ***
Library         SeleniumLibrary
##Resource        ../Resource/ResourceCommonLoginEva.robot
##Resource        ../Resource/PCP.robot
Resource          ../Resource/MSetor1pedido.robot
##Suite Setup      Open Browser To Login valid

*** Test Cases ***

CENARIO 1: Criar Tarefa por Setor com 1 pedido
  
    Dado que esteja logado na tela de PCP
    E buscar por pedido no filtro de pedido
    Quando selecionar pedido
    E clicar em tarefa por Setor

CENARIO 2: Criar Missao com 1 pedido
    
    Dado que acesse menu de Tarefas
    E busque pelo pedido
    E selecione o pedido
    Quando clicar no botao Criar
    Então a Missao devera ser criada com sucesso
    



    
