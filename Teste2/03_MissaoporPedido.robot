*** Settings ***
Library         SeleniumLibrary
Resource        ../Resource/ResourceCommonLoginEva.robot
Resource        ../Resource/MissaoPedido.robot

*** Test Cases ***
CENARIO 3: Criar Missao contendo Tarefa por Pedido

 Dado que esteja logado na tela de PCP
 E buscar por pedido no filtro de pedido
 Quando criar tarefa por pedido
 E selecionar pedido no menu de Tarefas em Aberto
 E criar a Missao
 Entao a Missão deverá ser exibida com sucesso


